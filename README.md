![Terlik Academy](https://database.bluecherrystudio.com/Terlik-Academy/Terlik-Academy-Logo-2.jpg)

- Aplication for education. Users can register and apply for courses. Admins can edin courses and users.

- We use: **TypeScript**, **Nest.js**, **Angular**, **TypeORM (data access)**, **MySQL/MariaDB** and **Jest**

# Functionalities and Project Description:

- **Public part** (accessible without authentication)
- **Private part** (available for registered users and admins)

### Public Part

- Users can **register** and **login**.

### Private Part

The private part could be accessed by **Users**, **Admins** and **SuperUsers**

- Users can:
  - **Edit** their profiles and basic data.
  - **Apply** for courses.
- Admins can:
  - **Add** and **edit** user data.
  - **Add** and **edit** course data.
  - **See** the uploaded applications and **details** about the applicants.
- SuperAdmins can:
  - Everything and **Admin** can do
  - **Change** users' types.

## **Installation**

### In order to be able to start the application you need to have mySQL installed.

### 1. Go to "backend" folder and run the following command "npm run migration:generate -- -n Init"

### 2. Then run "npm run migration:run"

### 3. Go to ormconfig.js file and set the connection to your DB server. Default values are:

- Hostname: **localhost**
- Username: **root**
- Password: **root**

### 4. Create mySQL schema named **terlik_academy**

### 5. In the **backend** folder write the following command: **_npm run start_** . The console should print **_Initial data added_** message.

### 6. Once the initial data is added you can use Postman to make **GET** and **POST** requests to the server

## **Backend routes. Description. Usage instructions.**

### **Public routes & requests**

- User registration route. Used for registration of all user types - @POST request - http://localhost:3000/auth/register

```
Example input data:

    {
    `"first_name" : "Milena"
    "last_name" : "Ivanova"
    "email" : "Milena.I.Ivanova@dir.bg"
    "password" : "MegaSiln@P@r0la"
    "avatarUrl" : "Postman -> Body -> Form-data -> Field type"File". Then select your avatar"` - Optional field
    }
```

- User login route. Used for login of all user types - @POST request Login route- http://localhost:3000/auth/login

```
Example input data:

    {
    "email":"Milena.I.Ivanova@dir.bg",
    "password":"MegaSiln@P@r0la"
    }
```

### **Routes & requests which need authentication**

### Aceessible by the regular users

- User Data update by id route - @POST request - http://localhost:3000/users/:id/profile

```
Example input data:

    {
    "first_name":"Milena",
    "middle_name":"Petrova",
    "last_name":"Ivanova",
    "email":"MienaPetrova@dir.bg",
    "city_id":"07fadeb5-8c3b-4abc-a36d-ba04bd7cd62e",
    "gender_id":"24109092-09d6-401b-ac98-e850832ca6af",
    "date_of_birth":"2019-01-06 21:34:56.778675",
    "mobile_phone":"088555555555",
    "password":"MegaSiln@P@r0la",
    "avatarUrl":"private\\uploads\\images\\Milena.png"
    }
```

- Get all courses in th DB route - @GET request - http://localhost:3000/courses/

- Add new application route - @POST request - http://localhost:3000/applications/apply

```
Example input data:

    {
    "course_id" : "d35e8633-53bf-4668-9383-8aa1ca8d4aea",
    "user_id" : "451671a9-8dfe-41b5-8a73-c13155c98b94",
    "why_appling" : "This is why I am applying",
    "career_goals" : "These are my career goals",
    }
```

### Aceessible by the admins and superAdmin users

Functionalities accessible for the regular user but in admin route:

```
- User Data update by id route - @POST request - http://localhost:3000/admin/users/:id/profile
- Get all courses in th DB route - @GET request - http://localhost:3000/admin/courses/
```

- Get all users route - @GET request - http://localhost:3000/admins/users
- Add Course to the DB route - @POST request - http://localhost:3000/admins/courses/add

```
Example input data:

    {
    "course_name":"Knitting Stitches Fundamentals",
    "course_description":"It is doing to be a very nice course",
    "active":"1"
    }
```

- Edit course data route by id route - @POST request- http://localhost:3000/admins/courses/:id

```
Example input data:

    {
    "course_name":"Knitting Stitches 2019",
    "course_description":"It is OMG course",
    "active":"1"
    }
```

- Get all applications route - @GET request - http://localhost:3000/admins/applications

### SuperUsers have access to the above routes plus:

- Edit user type route - @POST request - http://localhost:3000/admins/users/639ee06d-cc6b-46a5-8369-99d241bd0b14/type

```
    {
    "user_role_id":"599a6d16-a84c-44cf-a129-e0d61c2f5fc6"
    }
```

### **Team Members of this project:**

- Danail Kirilov (@my.telerikacademy.com - vaitlspaak@gmail.com)
- Petar Todorov (@my.telerikacademy.com - petar.ptodorov@gmail.com)

### Gitlab repository:

- Link: https://gitlab.com/vaitlspaak/terlik-academy

# Aditional Info for Terlik Academy:

## TERLIK ACADEMY

- Knitting Stitches Academy

## Types of courses:

### 1 Knitting Stitches Fundamentals:

#### Content of this course:

- Different types of Yarn
- Knitting Needles 'My best frients'
- Scissors 'Why shoud we care about them'

### 2 Knitting Stitches Advanced:

#### Content of this course:

- Purl stitches
- Combinations of two or more knitting techniques
- Creating the stitch patterns

### 3 Knitting Stitches Patterns:

#### Content of this course:

- GARTER STITCH PATTERN
- STOCKINETTE STITCH PATTERN
- RIB STITCH PATTERN
- SEED STITCH PATTERN
- ANDALUSIAN STITCH PATTERN
- PIQUE RIB STITCH PATTERN
- DOUBLE MOSS STITCH PATTERN
- WAFFLE STITCH PATTERN
- CHEVRON SEED STITCH PATTERN
- DIAGONAL RIB STITCH PATTERN
- FLAG STITCH PATTERN
- DIAMOND BROCADE STITCH PATTERN
- BASKET WEAVE STITCH PATTERN
- GARTER CHECKERBOARD STITCH PATTERN
- CATERPILLAR STITCH PATTERN
- DIAGONAL CHEVRON ZIGZAG STITCH PATTERN
- TRIANGLE STITCH PATTERN
- REVERSE RIDGE STITCH PATTERN
- IRISH MOSS STITCH PATTERN
- HURDLE STITCH PATTERN

### Trainers who has admin access in this APP:

- baba Pena
- baba Tonka
- baba Elena
