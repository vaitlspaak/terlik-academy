import { GetAllApplicationsDTO } from './../models/applications/applications-get-all-applications.dto';
import { ApplicationsService } from './../applications/applications.service';
import { Application } from './../data/entities/applications.entity';
import { UserEditTypeDTO } from './../models/user/user-edit-type.dto';
import { CourseAddEditDTO } from './../models/courses/course-add.dto';
import { Course } from './../data/entities/course.entity';
import { CourseService } from './../courses/course.service';
import { GetUserDataByIdDTO } from './../models/user/get-userdata-by-id.dto';
import { GetAllUserDataDTO } from './../models/user/get-allUsers-data.dto';
import { UsersService } from './../users/users.service';
import { User } from './../data/entities/user.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UserUpdateDTO } from 'src/models/user/user-update.dto';

@Injectable()
export class AdminsService {
  constructor(
    private readonly usersService: UsersService,
    private readonly courseService: CourseService,
    private readonly applicationService: ApplicationsService,
  ) {}
  async getAllUsers(): Promise<GetAllUserDataDTO[]> {
    return await this.usersService.getAll();
  }
  async updateUserById(
    user: UserUpdateDTO,
    id: any,
  ): Promise<GetUserDataByIdDTO> {
    return this.usersService.updateUserById(user, id);
  }

  async showAllCourse(): Promise<Course[]> {
    return await this.courseService.showAllCourse();
  }

  async addCourse(courseData: CourseAddEditDTO) {
    return await this.courseService.addCourse(courseData);
  }

  async editCourse(courseData: CourseAddEditDTO, id: any) {
    return await this.courseService.editCourse(courseData, id);
  }

  async changeUserType(
    role: UserEditTypeDTO,
    id: string,
  ): Promise<GetUserDataByIdDTO> {
    return this.usersService.changeUserType(role, id);
  }

  async getAllApplications(): Promise<GetAllApplicationsDTO[]> {
    return this.applicationService.getAllApplications();
  }
}
