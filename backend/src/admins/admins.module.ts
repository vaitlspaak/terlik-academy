import { GetUserDataByIdDTO } from './../models/user/get-userdata-by-id.dto';
import { UsersModule } from './../users/users.module';
import { GetAllUserDataDTO } from './../models/user/get-allUsers-data.dto';
import { UsersService } from './../users/users.service';
import { GetApplicationByIdDTO } from './../models/applications/applications-get-application-by-id.dto';
import { GetAllApplicationsDTO } from './../models/applications/applications-get-all-applications.dto';
import { ApplicationsModule } from './../applications/applications.module';
import { ApplicationsService } from './../applications/applications.service';
import { CourseService } from './../courses/course.service';
import { AdminsController } from './admins.controller';
import { Course } from '../data/entities/course.entity';
import { AuthModule } from '../auth/auth.module';
import { CoreModule } from '../common/core/core.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { AdminsService } from 'src/admins/admins.service';
import { Repository } from 'typeorm';

@Module({
  imports: [
    TypeOrmModule.forFeature([Course]),
    CoreModule,
    AuthModule,
    UsersModule,
  ],
  providers: [
    AdminsService,
    CourseService,
    ApplicationsService,
    GetAllApplicationsDTO,
    GetApplicationByIdDTO,
    GetAllUserDataDTO,
    GetUserDataByIdDTO,
  ],
  controllers: [AdminsController],
  exports: [AdminsModule],
})
export class AdminsModule {}
