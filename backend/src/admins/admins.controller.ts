import { UsersService } from './../users/users.service';
import { Roles } from './../common/decorators/roles.decorator';
import { RolesGuard } from './../common/guards/roles/roles.guard';
import { GetAllApplicationsDTO } from './../models/applications/applications-get-all-applications.dto';
import { UserEditTypeDTO } from './../models/user/user-edit-type.dto';
import { CourseAddEditDTO } from './../models/courses/course-add.dto';
import { Course } from './../data/entities/course.entity';
import { GetUserDataByIdDTO } from './../models/user/get-userdata-by-id.dto';
import { GetAllUserDataDTO } from './../models/user/get-allUsers-data.dto';
import { Controller, Get, UseGuards, Post, Body, Param } from '@nestjs/common';
import { AdminsService } from './admins.service';
import { AuthGuard } from '@nestjs/passport';
import { AdminGuard } from 'src/common';
import { UserUpdateDTO } from 'src/models/user/user-update.dto';

@Controller('admins')
// @UseGuards(AuthGuard(), RolesGuard)
export class AdminsController {
  constructor(private readonly adminService: AdminsService) {}

  @Get('users')
  @Roles('Admin')
  @UseGuards(AuthGuard())
  // @UseGuards(AuthGuard())
  getAllUsers(): Promise<GetAllUserDataDTO[]> {
    return this.adminService.getAllUsers();
  }

  @Post('users/:id/profile')
  @Roles('Admin')
  updateUserById(
    @Body() user: UserUpdateDTO,
    @Param('id') param,
  ): Promise<GetUserDataByIdDTO> {
    return this.adminService.updateUserById(user, param);
  }

  @Post('users/:id/type')
  @Roles('SuperUser')
  editUserType(
    @Body() editedRole: UserEditTypeDTO,
    @Param('id') id,
  ): Promise<GetUserDataByIdDTO> {
    return this.adminService.changeUserType(editedRole, id);
  }

  @Get('courses')
  @Roles('Admin')
  showAllCourse(): Promise<Course[]> {
    return this.adminService.showAllCourse();
  }

  @Post('courses/add')
  @Roles('Admin')
  addCourse(@Body() data: CourseAddEditDTO): Promise<Course[]> {
    return this.adminService.addCourse(data);
  }

  @Post('courses/:id')
  @Roles('Admin')
  editCourse(
    @Body() data: CourseAddEditDTO,
    @Param('id') id,
  ): Promise<Course[]> {
    return this.adminService.editCourse(data, id);
  }

  @Get('applications')
  @Roles('Admin')
  getAllApplications(): Promise<GetAllApplicationsDTO[]> {
    return this.adminService.getAllApplications();
  }
}
