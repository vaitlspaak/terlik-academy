import { UsersModule } from './../users/users.module';
import { DbRequestService } from './../db-request/db-request.service';
import { GetAllUserDataDTO } from './../models/user/get-allUsers-data.dto';
import { GetUserDataByIdDTO } from './../models/user/get-userdata-by-id.dto';
import { UserRegisterDTO } from './../models/user/user-register.dto';
import { UsersService } from './../users/users.service';
import { Module } from '@nestjs/common';
import { DbInitService } from './db-init.service';

@Module({
  imports: [UsersModule],
  providers: [
    DbInitService,
    UsersService,
    UserRegisterDTO,
    GetUserDataByIdDTO,
    GetAllUserDataDTO,
    DbRequestService,
  ],
})
export class DbInitModule {}
