import { CourseModule } from './courses/course.module';
import { AdminsModule } from './admins/admins.module';
import { AppService } from './app.service';
import { AppController } from './app.controller';
import { Module, HttpModule } from '@nestjs/common';
import { ConfigModule } from './config/config.module';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { CoreModule } from './common/core/core.module';
import { DatabaseModule } from './database/database.module';
import { ApplicationsModule } from './applications/applications.module';
import { DbInitModule } from './db-init/db-init.module';
import { HttpExceptionFilter } from './common/filters/exeption-filter';
import { DbRequestService } from './db-request/db-request.service';

@Module({
  imports: [
    ConfigModule,
    HttpModule,
    AuthModule,
    DatabaseModule,
    UsersModule,
    CoreModule,
    DatabaseModule,
    AdminsModule,
    CourseModule,
    ApplicationsModule,
    DbInitModule,
  ],
  controllers: [AppController],
  providers: [AppService, HttpExceptionFilter, DbRequestService],
})
export class AppModule {}
