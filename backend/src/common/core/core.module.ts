import { UsersModule } from './../../users/users.module';
import { DbInitService } from './../../db-init/db-init.service';
import { DbRequestService } from './../../db-request/db-request.service';
import { GetApplicationByIdDTO } from './../../models/applications/applications-get-application-by-id.dto';
import { ApplicationsService } from './../../applications/applications.service';
import { GetAllApplicationsDTO } from './../../models/applications/applications-get-all-applications.dto';
import { GetUserDataByIdDTO } from './../../models/user/get-userdata-by-id.dto';
import { GetAllUserDataDTO } from './../../models/user/get-allUsers-data.dto';
import { ApplicationDataService } from './../../application-data/application-data.service';
import { Application } from './../../data/entities/applications.entity';
import { ApplicationData } from './../../data/entities/application-data.entity';
import { UserRoles } from './../../data/entities/user-roles.entity';
import { Course } from './../../data/entities/course.entity';
import { UsersService } from './../../users/users.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { User } from './../../data/entities/user.entity';
import { FileService } from './file.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      User,
      ApplicationData,
      Course,
      UserRoles,
      Application,
    ]),
  ],
  providers: [
    UsersService,
    ApplicationDataService,
    ApplicationsService,
    FileService,
    GetUserDataByIdDTO,
    GetAllUserDataDTO,
    GetAllApplicationsDTO,
    GetApplicationByIdDTO,
    DbRequestService,
    DbInitService,
  ],
  exports: [UsersService, ApplicationDataService, FileService],
})
export class CoreModule {}
