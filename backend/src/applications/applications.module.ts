import { GetApplicationByIdDTO } from './../models/applications/applications-get-application-by-id.dto';
import { GetAllApplicationsDTO } from './../models/applications/applications-get-all-applications.dto';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ApplicationData } from './../data/entities/application-data.entity';
import { CoreModule } from './../common/core/core.module';
import { AuthModule } from './../auth/auth.module';
import { Module } from '@nestjs/common';
import { ApplicationsService } from './applications.service';
import { ApplicationsController } from './applications.controller';

@Module({
  imports: [
    TypeOrmModule.forFeature([ApplicationData]),
    CoreModule,
    AuthModule,
  ],
  providers: [
    ApplicationsService,
    GetAllApplicationsDTO,
    GetApplicationByIdDTO,
  ],
  controllers: [ApplicationsController],
})
export class ApplicationsModule {}
