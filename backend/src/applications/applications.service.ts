import { GetApplicationByIdDTO } from './../models/applications/applications-get-application-by-id.dto';
import { GetAllApplicationsDTO } from './../models/applications/applications-get-all-applications.dto';
import { ApplicationDataService } from './../application-data/application-data.service';
import { Injectable } from '@nestjs/common';
import { Application } from './../data/entities/applications.entity';
import { ApplicationAddDTO } from './../models/applications/applications-add.dto';
import { User } from './../data/entities/user.entity';
import { Course } from './../data/entities/course.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, getConnection } from 'typeorm';
@Injectable()
export class ApplicationsService {
  constructor(
    private readonly getAllApplicationsDTO: GetAllApplicationsDTO,
    private readonly getApplicationByIdDTO: GetApplicationByIdDTO,
    private readonly applicataionDataService: ApplicationDataService,
    @InjectRepository(Application)
    private readonly applicationsRepository: Repository<Application>,
    @InjectRepository(Course)
    private readonly courseRepository: Repository<Course>,
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
  ) {}

  async getAllApplications(): Promise<GetAllApplicationsDTO[]> {
    const applications = await getConnection()
      .getRepository(Application)
      .createQueryBuilder('applications')
      .innerJoinAndSelect('applications.user_id', 'users')
      .innerJoinAndSelect('users.city_id', 'cities')
      .innerJoinAndSelect('users.gender_id', 'gender_types')
      .innerJoinAndSelect('users.user_role_id', 'user_roles')
      .innerJoinAndSelect('applications.course_id', 'courses')
      .innerJoinAndSelect('applications.applicationData_id', 'application_data')
      .getMany();
    return await this.getAllApplicationsDTO.getDTO(applications);
  }
  async getApplicationById(
    applicationId: string,
  ): Promise<GetApplicationByIdDTO> {
    const result = await getConnection()
      .getRepository(Application)
      .createQueryBuilder('applications')
      .innerJoinAndSelect('applications.user_id', 'users')
      .innerJoinAndSelect('users.city_id', 'cities')
      .innerJoinAndSelect('users.gender_id', 'gender_types')
      .innerJoinAndSelect('users.user_role_id', 'user_roles')
      .innerJoinAndSelect('applications.course_id', 'courses')
      .innerJoinAndSelect('applications.applicationData_id', 'application_data')
      .andWhere('applications.application_id = :id', { id: `${applicationId}` })
      .getOne();

    return this.getApplicationByIdDTO.getDTO(result);
  }

  async addApplication(applicationData: ApplicationAddDTO) {
    const storedApplicationData = await this.applicataionDataService.addApplicationData(
      applicationData,
    );

    const course = await this.courseRepository.findOne({
      where: { course_id: applicationData.course_id },
    });

    const user = await this.userRepository.findOne({
      where: { user_id: applicationData.user_id },
    });

    const newApplication = new Application();
    newApplication.user_id = [];
    newApplication.user_id.push(user);
    newApplication.course_id = [];
    newApplication.course_id.push(course);
    newApplication.applicationData_id = [];
    newApplication.applicationData_id.push(storedApplicationData);
    this.applicationsRepository.create(newApplication);
    await this.applicationsRepository.save(newApplication);
  }
}
