import { AuthGuard } from '@nestjs/passport';
import { Roles } from './../common/decorators/roles.decorator';
import { RolesGuard } from './../common/guards/roles/roles.guard';
import { GetAllApplicationsDTO } from './../models/applications/applications-get-all-applications.dto';
import { GetApplicationByIdDTO } from './../models/applications/applications-get-application-by-id.dto';
import { FileService } from './../common/core/file.service';
import { ApplicationAddDTO } from './../models/applications/applications-add.dto';
import { ApplicationsService } from './applications.service';
import { join } from 'path';
import { unlink } from 'fs';

import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  UseInterceptors,
  FileInterceptor,
  UploadedFile,
  ValidationPipe,
  UseGuards,
} from '@nestjs/common';

@Controller('applications')
// @UseGuards(AuthGuard(), RolesGuard)
export class ApplicationsController {
  constructor(private readonly applicationService: ApplicationsService) {}

  @Get(':id')
  @Roles('Admin', 'User')
  getApplicationById(@Param('id') id): Promise<GetApplicationByIdDTO> {
    return this.applicationService.getApplicationById(id);
  }

  @Post('apply')
  @Roles('User')
  @UseInterceptors(
    FileInterceptor('resume', {
      limits: FileService.fileLimit(1, 2 * 1024 * 1024),
      storage: FileService.storage(['private', 'uploads', 'CVs']),
      fileFilter: (req, file, cb) =>
        FileService.fileFilter(req, file, cb, '.pdf', '.doc', '.docx'),
    }),
  )
  async addApplication(
    @Body()
    data: ApplicationAddDTO,
    @UploadedFile()
    file,
  ): Promise<string> {
    const folder = join('.', 'private', 'uploads', 'CVs');
    if (!file) {
      data.resume = join(folder, 'default.pdf');
    } else {
      data.resume = join(folder, file.filename);
    }

    try {
      await this.applicationService.addApplication(data);
      return 'saved';
    } catch (error) {
      await new Promise((resolve, reject) => {
        // Delete the file if user not found
        if (file) {
          unlink(join('.', file.path), err => {
            if (err) {
              reject(error.message);
            }
            resolve();
          });
        }
        resolve();
      });

      return error.message;
    }
  }
}
