import { ApplicationDataService } from '../../application-data/application-data.service';
import { Repository } from 'typeorm';
import { ApplicationData } from '../../data/entities/application-data.entity';
import { ApplicationAddDTO } from '../../models/applications/applications-add.dto';

describe('ApplicationDataService tests', () => {

  describe('ApplicationDataService should', () => {

    it('not to Throw', async () => {
      
      // Arrange
      const applicationAddDTO: ApplicationAddDTO = new ApplicationAddDTO();
      const repository: Repository<ApplicationData> = new Repository();
      const applicationDataService: ApplicationDataService = new ApplicationDataService(repository);

      const spy: jest.SpyInstance = jest.spyOn(applicationDataService, 'addApplicationData');
      
      // Act
      applicationDataService.addApplicationData(applicationAddDTO);
      
      // Assert
      expect(spy).not.toThrow();
    });

    it('not to be Null', async () => {

      // Arrange
      const applicationAddDTO: ApplicationAddDTO = new ApplicationAddDTO();
      const repository: Repository<ApplicationData> = new Repository();
      const applicationDataService: ApplicationDataService = new ApplicationDataService(repository);
      
      const spy: jest.SpyInstance = jest.spyOn(applicationDataService, 'addApplicationData');
      
      // Act
      applicationDataService.addApplicationData(applicationAddDTO);
      
      // Assert
      expect(spy).not.toBeNull();
    });

    it('not to throw Error', async () => {

      // Arrange
      const applicationAddDTO: ApplicationAddDTO = new ApplicationAddDTO();
      const repository: Repository<ApplicationData> = new Repository();
      const applicationDataService: ApplicationDataService = new ApplicationDataService(repository);

      const spy: jest.SpyInstance = jest.spyOn(applicationDataService, 'addApplicationData');
      
      // Act
      applicationDataService.addApplicationData(applicationAddDTO);
      
      // Assert
      expect(spy).not.toThrowError();
    });

    it('to be called', async () => {

      // Arrange
      const applicationAddDTO: ApplicationAddDTO = new ApplicationAddDTO();
      const repository: Repository<ApplicationData> = new Repository();
      const applicationDataService: ApplicationDataService = new ApplicationDataService(repository);
      
      const spy: jest.SpyInstance = jest.spyOn(applicationDataService, 'addApplicationData');
      
      // Act
      applicationDataService.addApplicationData(applicationAddDTO);
      
      // Assert
      expect(spy).toBeCalled();
    });

    it('to be called 1 time', async () => {

      // Arrange
      const applicationAddDTO: ApplicationAddDTO = new ApplicationAddDTO();
      const repository: Repository<ApplicationData> = new Repository();
      const applicationDataService: ApplicationDataService = new ApplicationDataService(repository);
      
      const spy: jest.SpyInstance = jest.spyOn(applicationDataService, 'addApplicationData');
      
      // Act
      applicationDataService.addApplicationData(applicationAddDTO);
      
      // Assert
      expect(spy).toBeCalledTimes(1);
    });
  });
});