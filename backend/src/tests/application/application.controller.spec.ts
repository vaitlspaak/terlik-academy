import { GetApplicationByIdDTO } from './../../models/applications/applications-get-application-by-id.dto';
import { GetAllApplicationsDTO } from './../../models/applications/applications-get-all-applications.dto';
import { ApplicationsController } from './../../applications/applications.controller';
import { ApplicationAddDTO } from './../../models/applications/applications-add.dto';
import { ApplicationsService } from './../../applications/applications.service';
import { ApplicationDataService } from './../../application-data/application-data.service';
import { Repository } from 'typeorm';
import { ApplicationData } from '../../data/entities/application-data.entity';
import { Application } from '../../data/entities/applications.entity';
import { User } from '../../data/entities/user.entity';
import { Course } from '../../data/entities/course.entity';
jest.mock('./../../applications/applications.service');

const userRepository: Repository<User> = new Repository();
const courseRepository: Repository<Course> = new Repository();
const applicationRepository: Repository<ApplicationData> = new Repository();
const applicationDataService: ApplicationDataService = new ApplicationDataService(
  applicationRepository,
);
const getAllApplicationsDTO: GetAllApplicationsDTO = new GetAllApplicationsDTO();
const getApplicationByIdDTO: GetApplicationByIdDTO = new GetApplicationByIdDTO();
const repositoryApplication: Repository<Application> = new Repository();
const applicationsService: ApplicationsService = new ApplicationsService(
  getAllApplicationsDTO,
  getApplicationByIdDTO,
  applicationDataService,
  repositoryApplication,
  courseRepository,
  userRepository,
);

const applicationsController: ApplicationsController = new ApplicationsController(
  applicationsService,
);

describe('ApplicationsController tests', () => {
  describe('GetAllApplications method should', () => {
    it('not to Throw', async () => {
      // Arrange
      const spy: jest.SpyInstance = jest.spyOn(
        applicationsService,
        'getAllApplications',
      );

      // Act
      applicationsService.getAllApplications();

      // Assert
      expect(spy).not.toThrow();
    });

    it('not to be Null', async () => {
      // Arrange
      const spy: jest.SpyInstance = jest.spyOn(
        applicationsService,
        'getAllApplications',
      );

      // Act
      applicationsService.getAllApplications();

      // Assert
      expect(spy).not.toBeNull();
    });

    it('not to throw Error', async () => {
      // Arrange

      const spy: jest.SpyInstance = jest.spyOn(
        applicationsService,
        'getAllApplications',
      );

      // Act
      applicationsService.getAllApplications();

      // Assert
      expect(spy).not.toThrowError();
    });

    it('to have been called', async () => {
      // Arrange
      const spy: jest.SpyInstance = jest.spyOn(
        applicationsService,
        'getAllApplications',
      );

      // Act
      applicationsService.getAllApplications();

      // Assert
      expect(spy).toHaveBeenCalled();
    });

    it('to have been called 1 time', async () => {
      // Arrange
      const applicationsService: ApplicationsService = new ApplicationsService(
        getAllApplicationsDTO,
        getApplicationByIdDTO,
        applicationDataService,
        repositoryApplication,
        courseRepository,
        userRepository,
      );

      const spy: jest.SpyInstance = jest.spyOn(
        applicationsService,
        'getAllApplications',
      );

      // Act
      applicationsService.getAllApplications();

      // Assert
      expect(spy).toBeCalledTimes(1);
    });
  });

  describe('getApplicationById method should', () => {
    it('to have been called 3 times', async () => {
      const spy: jest.SpyInstance = jest.spyOn(
        applicationsController,
        'getApplicationById',
      );

      // Act
      applicationsController.getApplicationById(null);
      applicationsController.getApplicationById(null);
      applicationsController.getApplicationById(null);

      // Assert
      expect(spy).toBeCalledTimes(3);
    });
  });

  describe('AddApplication method should', () => {
    it('to have been called', async () => {
      // Arrange

      const applicationDataToSubmit = new ApplicationAddDTO();
      applicationDataToSubmit.course_id = new Course();
      applicationDataToSubmit.user_id = new User();
      applicationDataToSubmit.why_appling = 'Reason to apply';
      applicationDataToSubmit.resume = 'Reason to apply';
      applicationDataToSubmit.career_goals = 'Reason to apply';

      const add: jest.SpyInstance = jest.spyOn(
        applicationsController,
        'addApplication',
      );
      // console.log(add);

      // Act
      let result = await applicationsController.addApplication(
        applicationDataToSubmit,
        null,
      );

      // Assert
      expect(add).toHaveBeenCalled();
    });

    it('to return "saved"', async () => {
      // Arrange
      const applicationDataToSubmit = new ApplicationAddDTO();
      applicationDataToSubmit.course_id = new Course();
      applicationDataToSubmit.user_id = new User();
      applicationDataToSubmit.why_appling = 'Reason to apply';
      applicationDataToSubmit.resume = 'Reason to apply';
      applicationDataToSubmit.career_goals = 'Reason to apply';

      // Act
      let result = await applicationsController.addApplication(
        applicationDataToSubmit,
        null,
      );

      // Assert
      expect(result).toBe('saved');
    });
  });
});
