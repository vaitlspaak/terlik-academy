import { CourseController } from './../../courses/course.controller';
import { CourseService } from '../../courses/course.service';
import { Repository } from 'typeorm';
import { Course } from '../../data/entities/course.entity';
import { CourseAddEditDTO } from '../../models/courses/course-add.dto';

describe('CourseController tests', () => {

    describe('showAllCourse method should', () => {
        it('to have been called', () => {
            let domMock: jest.Mock<CourseController>;
            const repository: Repository<Course> = new Repository();
            const courseService: CourseService = new CourseService(repository);
            // Arrange
            const spy: jest.SpyInstance = jest.spyOn(
                courseService,
                'showAllCourse',
            );
            // Act
            courseService.showAllCourse();
            // Assert
            expect(spy).toBeCalled();
        });
    });

    it('to have been called 1 time', () => {
        const repository: Repository<Course> = new Repository();
        const courseService: CourseService = new CourseService(repository);
        // Arrange
        const spy: jest.SpyInstance = jest.spyOn(
            courseService,
            'showAllCourse',
        );
        // Act
        courseService.showAllCourse();
        // Assert
        expect(spy).toBeCalledTimes(1);
    });

    it('not to throw', () => {
        const repository: Repository<Course> = new Repository();
        const courseService: CourseService = new CourseService(repository);
        // Arrange
        const spy: jest.SpyInstance = jest.spyOn(
            courseService,
            'showAllCourse',
        );
        // Act
        courseService.showAllCourse();
        // Assert
        expect(spy).not.toThrow();
    });

    it('not to throw Error', () => {
        const repository: Repository<Course> = new Repository();
        const courseService: CourseService = new CourseService(repository);
        // Arrange
        const spy: jest.SpyInstance = jest.spyOn(
            courseService,
            'showAllCourse',
        );
        // Act
        courseService.showAllCourse();
        // Assert
        expect(spy).not.toThrowError();
    });

    it('not to be Null', () => {
        const repository: Repository<Course> = new Repository();
        const courseService: CourseService = new CourseService(repository);
        // Arrange
        const spy: jest.SpyInstance = jest.spyOn(
            courseService,
            'showAllCourse',
        );
        // Act
        courseService.showAllCourse();
        // Assert
        expect(spy).not.toBeNull();
    });

    it('not to be Undefined', () => {
        const repository: Repository<Course> = new Repository();
        const courseService: CourseService = new CourseService(repository);
        // Arrange
        const spy: jest.SpyInstance = jest.spyOn(
            courseService,
            'showAllCourse',
        );
        // Act
        courseService.showAllCourse();
        // Assert
        expect(spy).not.toBeUndefined();
    });

    it('not to be NaN', () => {
        const repository: Repository<Course> = new Repository();
        const courseService: CourseService = new CourseService(repository);
        // Arrange
        const spy: jest.SpyInstance = jest.spyOn(
            courseService,
            'showAllCourse',
        );
        // Act
        courseService.showAllCourse();
        // Assert
        expect(spy).not.toBeNaN();
    });
});

describe('addCourse method should', () => {
    it('to have been called', () => {

        const courseData: CourseAddEditDTO = new CourseAddEditDTO;
        courseData.active = true;
        courseData.course_description = 'some discription';
        courseData.course_name = 'some name'
        const repository: Repository<Course> = new Repository();
        const courseService: CourseService = new CourseService(repository);
        // Arrange
        const spy: jest.SpyInstance = jest.spyOn(
            courseService,
            'addCourse',
        );
        // Act
        courseService.addCourse(courseData);
        // Assert
        expect(spy).toBeCalled();
    });

    it('to have been called 1 time', () => {

        const courseData: CourseAddEditDTO = new CourseAddEditDTO;
        courseData.active = true;
        courseData.course_description = 'some discription';
        courseData.course_name = 'some name'
        const repository: Repository<Course> = new Repository();
        const courseService: CourseService = new CourseService(repository);
        // Arrange
        const spy: jest.SpyInstance = jest.spyOn(
            courseService,
            'addCourse',
        );
        // Act
        courseService.addCourse(courseData);
        // Assert
        expect(spy).toBeCalledTimes(1);
    });

    it('not to throw', () => {

        const courseData: CourseAddEditDTO = new CourseAddEditDTO;
        courseData.active = true;
        courseData.course_description = 'some discription';
        courseData.course_name = 'some name'
        const repository: Repository<Course> = new Repository();
        const courseService: CourseService = new CourseService(repository);
        // Arrange
        const spy: jest.SpyInstance = jest.spyOn(
            courseService,
            'addCourse',
        );
        // Act
        courseService.addCourse(courseData);
        // Assert
        expect(spy).not.toThrow();
    });

    it('not to throw Error', () => {

        const courseData: CourseAddEditDTO = new CourseAddEditDTO;
        courseData.active = true;
        courseData.course_description = 'some discription';
        courseData.course_name = 'some name'
        const repository: Repository<Course> = new Repository();
        const courseService: CourseService = new CourseService(repository);
        // Arrange
        const spy: jest.SpyInstance = jest.spyOn(
            courseService,
            'addCourse',
        );
        // Act
        courseService.addCourse(courseData);
        // Assert
        expect(spy).not.toThrowError();
    });

    it('not to be NaN', () => {

        const courseData: CourseAddEditDTO = new CourseAddEditDTO;
        courseData.active = true;
        courseData.course_description = 'some discription';
        courseData.course_name = 'some name'
        const repository: Repository<Course> = new Repository();
        const courseService: CourseService = new CourseService(repository);
        // Arrange
        const spy: jest.SpyInstance = jest.spyOn(
            courseService,
            'addCourse',
        );
        // Act
        courseService.addCourse(courseData);
        // Assert
        expect(spy).not.toBeNaN();
    });

    it('not to be Undefined', () => {

        const courseData: CourseAddEditDTO = new CourseAddEditDTO;
        courseData.active = true;
        courseData.course_description = 'some discription';
        courseData.course_name = 'some name'
        const repository: Repository<Course> = new Repository();
        const courseService: CourseService = new CourseService(repository);
        // Arrange
        const spy: jest.SpyInstance = jest.spyOn(
            courseService,
            'addCourse',
        );
        // Act
        courseService.addCourse(courseData);
        // Assert
        expect(spy).not.toBeUndefined();
    });

    it('not to be null', () => {

        const courseData: CourseAddEditDTO = new CourseAddEditDTO;
        courseData.active = true;
        courseData.course_description = 'some discription';
        courseData.course_name = 'some name'
        const repository: Repository<Course> = new Repository();
        const courseService: CourseService = new CourseService(repository);
        // Arrange
        const spy: jest.SpyInstance = jest.spyOn(
            courseService,
            'addCourse',
        );
        // Act
        courseService.addCourse(courseData);
        // Assert
        expect(spy).not.toBeNull();
    });

    describe('editCourse method should', () => {

        it('to have been called', () => {

            const courseData: CourseAddEditDTO = new CourseAddEditDTO;
            courseData.active = true;
            courseData.course_description = 'some discription';
            courseData.course_name = 'some name'
            const repository: Repository<Course> = new Repository();
            const courseService: CourseService = new CourseService(repository);
            // Arrange
            const spy: jest.SpyInstance = jest.spyOn(
                courseService,
                'editCourse',
            );
            // Act
            courseService.editCourse(courseData, null);
            // Assert
            expect(spy).toBeCalled();
        });

        it('to have been called 1 time', () => {

            const courseData: CourseAddEditDTO = new CourseAddEditDTO;
            courseData.active = true;
            courseData.course_description = 'some discription';
            courseData.course_name = 'some name'
            const repository: Repository<Course> = new Repository();
            const courseService: CourseService = new CourseService(repository);
            // Arrange
            const spy: jest.SpyInstance = jest.spyOn(
                courseService,
                'editCourse',
            );
            // Act
            courseService.editCourse(courseData, null);
            // Assert
            expect(spy).toBeCalledTimes(1);
        });

        it('not to trow', () => {

            const courseData: CourseAddEditDTO = new CourseAddEditDTO;
            courseData.active = true;
            courseData.course_description = 'some discription';
            courseData.course_name = 'some name'
            const repository: Repository<Course> = new Repository();
            const courseService: CourseService = new CourseService(repository);
            // Arrange
            const spy: jest.SpyInstance = jest.spyOn(
                courseService,
                'editCourse',
            );
            // Act
            courseService.editCourse(courseData, null);
            // Assert
            expect(spy).not.toThrow();
        });

        it('not to throw error', () => {

            const courseData: CourseAddEditDTO = new CourseAddEditDTO;
            courseData.active = true;
            courseData.course_description = 'some discription';
            courseData.course_name = 'some name'
            const repository: Repository<Course> = new Repository();
            const courseService: CourseService = new CourseService(repository);
            // Arrange
            const spy: jest.SpyInstance = jest.spyOn(
                courseService,
                'editCourse',
            );
            // Act
            courseService.editCourse(courseData, null);
            // Assert
            expect(spy).not.toThrowError();
        });

        it('not to be NaN', () => {

            const courseData: CourseAddEditDTO = new CourseAddEditDTO;
            courseData.active = true;
            courseData.course_description = 'some discription';
            courseData.course_name = 'some name'
            const repository: Repository<Course> = new Repository();
            const courseService: CourseService = new CourseService(repository);
            // Arrange
            const spy: jest.SpyInstance = jest.spyOn(
                courseService,
                'editCourse',
            );
            // Act
            courseService.editCourse(courseData, null);
            // Assert
            expect(spy).not.toBeNaN();
        });

        it('not to be Undefined', () => {

            const courseData: CourseAddEditDTO = new CourseAddEditDTO;
            courseData.active = true;
            courseData.course_description = 'some discription';
            courseData.course_name = 'some name'
            const repository: Repository<Course> = new Repository();
            const courseService: CourseService = new CourseService(repository);
            // Arrange
            const spy: jest.SpyInstance = jest.spyOn(
                courseService,
                'editCourse',
            );
            // Act
            courseService.editCourse(courseData, null);
            // Assert
            expect(spy).not.toBeUndefined();
        });

        it('not to be Null', () => {

            const courseData: CourseAddEditDTO = new CourseAddEditDTO;
            courseData.active = true;
            courseData.course_description = 'some discription';
            courseData.course_name = 'some name'
            const repository: Repository<Course> = new Repository();
            const courseService: CourseService = new CourseService(repository);
            // Arrange
            const spy: jest.SpyInstance = jest.spyOn(
                courseService,
                'editCourse',
            );
            // Act
            courseService.editCourse(courseData, null);
            // Assert
            expect(spy).not.toBeNull();
        });

        it('not to be Null', () => {

            const courseData: CourseAddEditDTO = new CourseAddEditDTO;
            courseData.active = true;
            courseData.course_description = 'some discription';
            courseData.course_name = 'some name'
            const repository: Repository<Course> = new Repository();
            const courseService: CourseService = new CourseService(repository);
            // Arrange
            const spy: jest.SpyInstance = jest.spyOn(
                courseService,
                'editCourse',
            );
            // Act
            courseService.editCourse(courseData, null);
            // Assert
            expect(spy).not.toBeNull();
        });
    });
});
