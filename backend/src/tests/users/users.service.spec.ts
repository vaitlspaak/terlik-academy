import { GenderTypes } from './../../data/entities/gender-types.entity';
import { Application } from './../../data/entities/applications.entity';
import { UserRoles } from './../../data/entities/user-roles.entity';
import { User } from './../../data/entities/user.entity';
import { Repository } from 'typeorm';
import { DbRequestService } from './../../db-request/db-request.service';
import { GetUserDataByIdDTO } from './../../models/user/get-userdata-by-id.dto';
import { GetAllUserDataDTO } from './../../models/user/get-allUsers-data.dto';
import { UsersService } from './../../users/users.service';
jest.mock('./../../users/users.service');
jest.mock('./../../models/user/get-allUsers-data.dto');
jest.mock('./../../models/user/get-userdata-by-id.dto');
jest.mock('./../../db-request/db-request.service');

describe('Users service methods', () => {
  describe('getAll method should', () => {
    it('return all users', async () => {
      //Arrange
      const getAllUserData: GetAllUserDataDTO = new GetAllUserDataDTO();
      const getUserDataById: GetUserDataByIdDTO = new GetUserDataByIdDTO();
      const dbRequest: DbRequestService = new DbRequestService();
      const userRepository: Repository<User> = new Repository();
      const userRolesRepository: Repository<UserRoles> = new Repository();

      let usersService: UsersService = new UsersService(
        getUserDataById,
        getAllUserData,
        dbRequest,
        userRepository,
        userRolesRepository,
      );

      const applications: Application[] = [];
      const genderTypes = new GenderTypes();
      const date = new Date();
      const userRole = new UserRoles();
      const usersArr: User[] = [
        {
          user_id: applications,
          first_name: 'MIlena',
          middle_name: 'MIlena',
          last_name: 'Mileva',
          email: 'MilenaMil@gmail.com',
          gender_id: genderTypes,
          date_of_birth: date,
          mobile_number: null,
          city_id: null,
          registration_date: date,
          user_role_id: userRole,
          password: 'Meg@Par0l@',
          avatarUrl: 'uploads/avatar',
        },
        {
          user_id: applications,
          first_name: 'Genadi',
          middle_name: 'Ivanov',
          last_name: 'Petrov',
          email: 'g.petrov@gmail.com',
          gender_id: genderTypes,
          date_of_birth: date,
          mobile_number: null,
          city_id: null,
          registration_date: date,
          user_role_id: userRole,
          password: 'Meg@Par0l@',
          avatarUrl: 'uploads/avatar',
        },
      ];
      const getAllUsersDataMock: jest.SpyInstance = jest
        .spyOn(usersService, 'getAll')
        .mockImplementation(() => usersArr);

      //Act
      const result = await usersService.getAll();

      //Assert
      expect(result).toBe(usersArr);
    });

    it('return user based on their id', async () => {
      //Arrange
      const getAllUserData: GetAllUserDataDTO = new GetAllUserDataDTO();
      const getUserDataById: GetUserDataByIdDTO = new GetUserDataByIdDTO();
      const dbRequest: DbRequestService = new DbRequestService();
      const userRepository: Repository<User> = new Repository();
      const userRolesRepository: Repository<UserRoles> = new Repository();

      let usersService: UsersService = new UsersService(
        getUserDataById,
        getAllUserData,
        dbRequest,
        userRepository,
        userRolesRepository,
      );

      const applications: Application[] = [];
      const genderTypes = new GenderTypes();
      const date = new Date();
      const userRole = new UserRoles();
      const usersArr: User[] = [
        {
          user_id: applications,
          first_name: 'MIlena',
          middle_name: 'MIlena',
          last_name: 'Mileva',
          email: 'MilenaMil@gmail.com',
          gender_id: genderTypes,
          date_of_birth: date,
          mobile_number: null,
          city_id: null,
          registration_date: date,
          user_role_id: userRole,
          password: 'Meg@Par0l@',
          avatarUrl: 'uploads/avatar',
        },
        {
          user_id: applications,
          first_name: 'Genadi',
          middle_name: 'Ivanov',
          last_name: 'Petrov',
          email: 'g.petrov@gmail.com',
          gender_id: genderTypes,
          date_of_birth: date,
          mobile_number: null,
          city_id: null,
          registration_date: date,
          user_role_id: userRole,
          password: 'Meg@Par0l@',
          avatarUrl: 'uploads/avatar',
        },
      ];
      const getUsersDataByIdMock: jest.SpyInstance = jest
        .spyOn(usersService, 'getUserData')
        .mockImplementation(() => usersArr[0]);

      //Act
      const result = await usersService.getUserData(
        JSON.stringify(usersArr[0].user_id),
      );

      //Assert
      expect(result).toBe(usersArr[0]);
    });
  });
});
