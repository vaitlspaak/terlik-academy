import { Roles } from './../common/decorators/roles.decorator';
import { RolesGuard } from './../common/guards/roles/roles.guard';
import { GetAllUserDataDTO } from './../models/user/get-allUsers-data.dto';
import { GetUserDataByIdDTO } from './../models/user/get-userdata-by-id.dto';
import { UserUpdateDTO } from './../models/user/user-update.dto';
import { UsersService } from './users.service';
import { Observable } from 'rxjs';
import { AuthGuard } from '@nestjs/passport';
import { Controller, Get, Param, Post, Body, UseGuards } from '@nestjs/common';
@Controller('users')
// @UseGuards(AuthGuard(), RolesGuard)
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Get()
  @Roles('Admin')
  // @UseGuards(AuthGuard(), AdminGuard)
  all(): Promise<GetAllUserDataDTO[]> {
    return this.usersService.getAll();
  }

  @Get(':id/profile')
  @Roles('User', 'Admin')
  getUserDataById(@Param('id') param): Promise<GetUserDataByIdDTO> {
    console.log(param);
    return this.usersService.getUserData(param);
  }

  @Post(':id/profile')
  @Roles('User', 'Admin')
  updateUserById(
    @Body() user: UserUpdateDTO,
    @Param('id') param,
  ): Promise<GetUserDataByIdDTO> {
    return this.usersService.updateUserById(user, param);
  }
}
