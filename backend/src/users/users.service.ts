import { UserEditTypeDTO } from './../models/user/user-edit-type.dto';
import { DbRequestService } from './../db-request/db-request.service';
import { GetUserDataByIdDTO } from './../models/user/get-userdata-by-id.dto';
import { GetAllUserDataDTO } from './../models/user/get-allUsers-data.dto';
import { UserRoles } from './../data/entities/user-roles.entity';
import { UserUpdateDTO } from './../models/user/user-update.dto';
import { User } from './../data/entities/user.entity';
import { GetUserDTO } from '../models/user/get-user.dto';
import { UserLoginDTO } from '../models/user/user-login.dto';
import { UserRegisterDTO } from '../models/user/user-register.dto';
import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

import * as bcrypt from 'bcrypt';
import { JwtPayload } from '../interfaces/jwt-payload';
import { getConnection } from 'typeorm';

@Injectable()
export class UsersService {
  constructor(
    private readonly getUserDataByIdDTO: GetUserDataByIdDTO,
    private readonly getAllUserDataDTO: GetAllUserDataDTO,
    private readonly dbRequestService: DbRequestService,
    @InjectRepository(User)
    private readonly usersRepository: Repository<User>,
    @InjectRepository(UserRoles)
    private readonly userRolesRepository: Repository<UserRoles>,
  ) {}
  async registerUser(user: UserRegisterDTO) {
    user.user_role_id = await this.dbRequestService.getUserTypeId('User');

    user.city_id = await this.dbRequestService.getDefaultCityId('No city set');

    user.gender_id = await this.dbRequestService.getDefaultGenderTypeId(
      'No gender set',
    );

    console.log(user);

    const userFound = await this.usersRepository.findOne({
      where: { email: user.email },
    });
    if (userFound) {
      throw new Error('User with this email already exist in the DB!');
    }

    user.password = await bcrypt.hash(user.password, 10);
    await this.usersRepository.create(user);
    const result = await this.usersRepository.save([user]);
    return result;
  }
  async validateUser(payload: JwtPayload) {
    const userFound: any = await this.usersRepository.findOne({
      where: { email: payload.email },
    });

    const userRole = await getConnection()
      .getRepository(User)
      .createQueryBuilder('users')
      .innerJoinAndSelect('users.user_role_id', 'user_roles')
      .where('users.email = :email', { email: `${userFound.email}` })
      .getOne();

    // console.log(userRole);

    return { userFound, userRole };
  }
  async signIn(user: UserLoginDTO): Promise<GetUserDTO> {
    const userFound: GetUserDTO = await this.usersRepository.findOne({
      select: ['email', 'password'],
      where: { email: user.email },
    });
    if (userFound) {
      const result = await bcrypt.compare(user.password, userFound.password);
      if (result) {
        return userFound;
      }
    }
    return null;
  }
  async getAll(): Promise<GetAllUserDataDTO[]> {
    const usersData = await getConnection()
      .getRepository(User)
      .createQueryBuilder('users')
      .innerJoinAndSelect('users.city_id', 'cities')
      .innerJoinAndSelect('users.gender_id', 'gender_types')
      .innerJoinAndSelect('users.user_role_id', 'user_roles')
      .getMany();

    return this.getAllUserDataDTO.getDTO(usersData);
  }

  async getUserData(receivedId: string): Promise<GetUserDataByIdDTO> {
    const result = await getConnection()
      .getRepository(User)
      .createQueryBuilder('users')
      .innerJoinAndSelect('users.city_id', 'cities')
      .innerJoinAndSelect('users.gender_id', 'gender_types')
      .innerJoinAndSelect('users.user_role_id', 'user_roles')
      .where('users.user_id= :id', { id: `${receivedId}` })
      .getOne();

    return this.getUserDataByIdDTO.getDTO(result);
  }

  async updateUserById(
    user: UserUpdateDTO,
    id: string,
  ): Promise<GetUserDataByIdDTO> {
    const userFound = await this.usersRepository.findOne({
      where: { user_id: id },
    });
    if (!userFound) {
      throw new Error('User not found!');
    }

    await this.usersRepository.update(id, user);
    return this.getUserData(id);
  }

  async changeUserType(
    role: UserEditTypeDTO,
    id: string,
  ): Promise<GetUserDataByIdDTO> {
    const userFound = await this.usersRepository.findOne({
      where: { user_id: id },
    });
    if (!userFound) {
      throw new Error('User not found!');
    }
    userFound.user_role_id = role.user_role_id;
    await this.usersRepository.update(id, userFound);
    return this.getUserData(id);
  }
}
