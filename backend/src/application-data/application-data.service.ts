import { ApplicationData } from './../data/entities/application-data.entity';
import { ApplicationAddDTO } from './../models/applications/applications-add.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';

@Injectable()
export class ApplicationDataService {
  constructor(
    @InjectRepository(ApplicationData)
    private readonly applicationDataRepository: Repository<ApplicationData>,
  ) {}
  async addApplicationData(applicationData: ApplicationAddDTO) {
    const newApplicationData = new ApplicationData();

    newApplicationData.career_goals = applicationData.career_goals;
    newApplicationData.exam_date = applicationData.exam_date;
    newApplicationData.why_appling = applicationData.why_appling;
    newApplicationData.resume = applicationData.resume;

    this.applicationDataRepository.create(newApplicationData);
    return await this.applicationDataRepository.save(newApplicationData);
  }
}
