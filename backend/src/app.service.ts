import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {

  mainRoute() {

    return 'This is the main page';
  }

  registerRoute() {

    return 'This is the register page';
  }

  loginRoute() {

    return 'This is the login page';
  }
}
