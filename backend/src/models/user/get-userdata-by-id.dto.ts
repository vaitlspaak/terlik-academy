import { User } from './../../data/entities/user.entity';
export class GetUserDataByIdDTO {
  FirstName: string;
  MiddleName: string;
  LastName: string;
  genderId: string;
  DateOfBirth: Date;
  email: string;
  MobileNumber: string;
  City: string;
  Gender: string;
  avatarUrl: string;

  constructor() {}
  public getDTO(userData: User) {
    const user: GetUserDataByIdDTO = new GetUserDataByIdDTO();
    user.FirstName = userData.first_name;
    user.MiddleName = userData.middle_name;
    user.LastName = userData.last_name;
    user.email = userData.email;
    user.MobileNumber = userData.mobile_number;
    user.DateOfBirth = userData.date_of_birth;
    user.City = userData.city_id.city_name;
    user.Gender = userData.gender_id.gender_type;
    user.avatarUrl = userData.avatarUrl;
    return user;
  }
}
