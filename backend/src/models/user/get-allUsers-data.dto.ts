import { User } from './../../data/entities/user.entity';
export class GetAllUserDataDTO {
  FirstName: string;
  MiddleName: string;
  LastName: string;
  genderId: string;
  DateOfBirth: Date;
  email: string;
  MobileNumber: string;
  City: string;
  Gender: string;
  avatarUrl: string;

  constructor() {}
  public getDTO(userData: User[]) {
    const usersDTO: GetAllUserDataDTO[] = [];
    userData.forEach(user => {
      const currentUser = new GetAllUserDataDTO();
      currentUser.FirstName = user.first_name;
      currentUser.MiddleName = user.middle_name;
      currentUser.LastName = user.last_name;
      currentUser.email = user.email;
      currentUser.MobileNumber = user.mobile_number;
      currentUser.DateOfBirth = user.date_of_birth;
      currentUser.City = user.city_id.city_name;
      currentUser.Gender = user.gender_id.gender_type;
      currentUser.avatarUrl = user.avatarUrl;
      usersDTO.push(currentUser);
    });

    return usersDTO;
  }
}
