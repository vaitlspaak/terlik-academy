import { GenderTypes } from './../../data/entities/gender-types.entity';
import { Cities } from './../../data/entities/cities-db.entity';
import { UserRoles } from './../../data/entities/user-roles.entity';
import { IsString, Length, IsEmail, IsAlpha } from 'class-validator';

export class UserRegisterDTO {
  @IsAlpha()
  @Length(2, 45)
  first_name: string;

  @IsAlpha()
  @Length(2, 45)
  last_name: string;

  @Length(5, 100)
  @IsEmail()
  email: string;

  @IsString()
  password: string;

  user_role_id: UserRoles;

  avatarUrl: string;

  city_id: Cities;

  gender_id: GenderTypes;
}
