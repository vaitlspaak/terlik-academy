import { UserRoles } from './../../data/entities/user-roles.entity';

export class UserEditTypeDTO {
  user_role_id: UserRoles;
}
