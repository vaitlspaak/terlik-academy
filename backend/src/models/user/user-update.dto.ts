import { GenderTypes } from './../../data/entities/gender-types.entity';
import { Cities } from './../../data/entities/cities-db.entity';
import { Length, IsEmail, IsAlpha, IsString, Matches } from 'class-validator';

export class UserUpdateDTO {
  @IsAlpha()
  @Length(2, 45)
  first_name: string;

  @IsAlpha()
  @Length(2, 45)
  middle_name: string;

  @IsAlpha()
  @Length(2, 45)
  last_name: string;

  @Length(5, 100)
  @IsEmail()
  email: string;

  city_id: Cities;

  gender_id: GenderTypes;
  date_of_birth: Date;
  mobile_phone: string;
  avatarUrl: string;

  @IsString()
  @Matches(/(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,}/)
  password: string;
}
