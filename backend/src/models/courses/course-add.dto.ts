import { UserRoles } from './../../data/entities/user-roles.entity';
import {
  IsString,
  Length,
  Matches,
  IsOptional,
  IsEmail,
  IsAlpha,
  IsUUID,
  IsNumber,
} from 'class-validator';
export class CourseAddEditDTO {
  @IsAlpha()
  @Length(2, 30)
  course_name: string;

  @IsAlpha()
  @Length(2, 100)
  course_description: string;

  @IsNumber()
  active: boolean;
}
