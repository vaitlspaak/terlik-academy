import { Application } from './../../data/entities/applications.entity';
export class GetApplicationByIdDTO {
  FirstName: string;
  LastName: string;
  Email: string;
  Phone: string;
  City: string;
  CourseName: string;
  ExamResult: number;
  Photo: string;

  WhyApplying: string;
  CareerGoals: string;
  Resume: string;

  ExamDate: Date;

  public getDTO(application: Application) {
    const formatedApplication: GetApplicationByIdDTO = new GetApplicationByIdDTO();
    formatedApplication.FirstName = application.user_id[0].first_name;
    formatedApplication.LastName = application.user_id[0].last_name;
    formatedApplication.Email = application.user_id[0].email;
    formatedApplication.Phone = application.user_id[0].mobile_number;
    formatedApplication.City = application.user_id[0].city_id.city_name;
    formatedApplication.CourseName = application.course_id[0].course_name;
    formatedApplication.ExamResult =
      application.applicationData_id[0].exam_result;
    formatedApplication.Photo = application.user_id[0].avatarUrl;
    formatedApplication.WhyApplying =
      application.applicationData_id[0].why_appling;
    formatedApplication.CareerGoals =
      application.applicationData_id[0].career_goals;
    formatedApplication.Resume = application.applicationData_id[0].resume;
    formatedApplication.ExamDate = application.applicationData_id[0].exam_date;

    return formatedApplication;
  }
}
