import { Application } from './../../data/entities/applications.entity';
export class GetAllApplicationsDTO {
  FirstName: string;
  LastName: string;
  Email: string;
  CourseName: string;
  ExamResult: number;

  public getDTO(applications: Application[]) {
    const formatedApplications: GetAllApplicationsDTO[] = [];

    applications.forEach(application => {
      let currentApp: GetAllApplicationsDTO = new GetAllApplicationsDTO();
      currentApp.FirstName = application.user_id[0].first_name;
      currentApp.LastName = application.user_id[0].last_name;
      currentApp.Email = application.user_id[0].email;
      currentApp.CourseName = application.course_id[0].course_name;
      currentApp.ExamResult = application.applicationData_id[0].exam_result;

      formatedApplications.push(currentApp);
    });

    return formatedApplications;
  }
}
