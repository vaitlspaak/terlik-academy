import { User } from './../../data/entities/user.entity';
import { Course } from './../../data/entities/course.entity';
export class ApplicationAddDTO {
  course_id: Course;
  user_id: User;
  exam_date: Date;
  why_appling: string;
  career_goals: string;
  resume: string;
}
