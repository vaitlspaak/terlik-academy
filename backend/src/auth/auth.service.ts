import { User } from './../data/entities/user.entity';
import { UserRoles } from './../data/entities/user-roles.entity';
import { getConnection } from 'typeorm';
import { UsersService } from './../users/users.service';
import { GetUserDTO } from '../models/user/get-user.dto';
import { UserLoginDTO } from '../models/user/user-login.dto';
import { JwtService } from '@nestjs/jwt';
import { Injectable } from '@nestjs/common';
import { JwtPayload } from './../interfaces/jwt-payload';

@Injectable()
export class AuthService {
  constructor(
    private readonly usersService: UsersService,
    private readonly jwtService: JwtService,
  ) {}

  public async signIn(user: UserLoginDTO): Promise<string> {
    const userFound: GetUserDTO = await this.usersService.signIn(user);

    if (userFound) {
      return this.jwtService.sign({
        email: userFound.email,
        password: userFound.password,
      });
    } else {
      return null;
    }
  }

  async validateUser(payload: JwtPayload) {
    return await this.usersService.validateUser(payload);
  }
}
