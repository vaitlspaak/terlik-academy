import { DbInitService } from './db-init/db-init.service';
import { ConfigService } from './config/config.service';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { join } from 'path';
import * as cors from 'cors';
import { HttpExceptionFilter } from './common/filters/exeption-filter';

const dbInitService = new DbInitService();

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  // app.useGlobalFilters(new HttpExceptionFilter);
  app.use(cors());
  app.useStaticAssets(join(__dirname, '..', 'public'));
  await app.listen(app.get(ConfigService).port);

  if (!(await dbInitService.dbCheckIfInitialized())) {
    await dbInitService.dbInit();
  } else {
    console.log('Initial data has been already imported');
  }
}
bootstrap();
