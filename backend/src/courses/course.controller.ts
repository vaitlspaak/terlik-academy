import { AuthGuard } from '@nestjs/passport';
import { Course } from './../data/entities/course.entity';
import { CourseAddEditDTO } from './../models/courses/course-add.dto';
import { Controller, Get, Body, Post, Param, UseGuards } from '@nestjs/common';
import { CourseService } from './course.service';

@Controller('courses')
export class CourseController {
  constructor(private readonly coursesService: CourseService) {}

  @Get('')
  // @UseGuards(AuthGuard())
  showAllCourse(): Promise<Course[]> {
    return this.coursesService.showAllCourse();
  }

  // @Post('add')
  // addCourse(@Body() data: CourseAddEditDTO): Promise<Course[]> {
  //   return this.coursesService.addCourse(data);
  // }

  // @Post(':id')
  // editCourse(
  //   @Body() data: CourseAddEditDTO,
  //   @Param('id') id,
  // ): Promise<Course[]> {
  //   return this.coursesService.editCourse(data, id);
  // }
}
