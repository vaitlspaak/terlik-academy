import { Course } from './../data/entities/course.entity';
import { AuthModule } from './../auth/auth.module';
import { CoreModule } from './../common/core/core.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { CourseController } from './course.controller';
import { CourseService } from './course.service';

@Module({
  imports: [TypeOrmModule.forFeature([Course]), CoreModule, AuthModule],
  controllers: [CourseController],
  providers: [CourseService],
  exports: [CourseModule],
})
export class CourseModule {}
