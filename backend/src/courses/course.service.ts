import { CourseAddEditDTO } from './../models/courses/course-add.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Course } from './../data/entities/course.entity';
import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';

@Injectable()
export class CourseService {
  constructor(
    @InjectRepository(Course)
    private readonly coursesRepository: Repository<Course>,
  ) {}

  async showAllCourse(): Promise<Course[]> {
    return this.coursesRepository.find({});
  }

  async addCourse(courseData: CourseAddEditDTO): Promise<Course[]> {
    const courseFound = await this.coursesRepository.findOne({
      where: { course_name: courseData.course_name },
    });
    if (courseFound) {
      throw new Error('This course has been already added!');
    }
    await this.coursesRepository.create(courseData);
    await this.coursesRepository.save([courseData]);
    return this.showAllCourse();
  }

  async editCourse(courseData: CourseAddEditDTO, id: any): Promise<Course[]> {
    const courseFound = await this.coursesRepository.findOne({
      where: { course_id: id },
    });
    if (!courseFound) {
      throw new Error('This course cannot be found');
    }
    await this.coursesRepository.update(id, courseData);
    return this.showAllCourse();
  }
}
