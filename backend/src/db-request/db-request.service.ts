import { GenderTypes } from './../data/entities/gender-types.entity';
import { Cities } from './../data/entities/cities-db.entity';
import { UserRoles } from './../data/entities/user-roles.entity';
import { getConnection } from 'typeorm';
import { Injectable } from '@nestjs/common';

@Injectable()
export class DbRequestService {
  async getUserTypeId(userRole: string): Promise<UserRoles> {
    const userRoleId = await getConnection()
      .createQueryBuilder(UserRoles, 'user_roles')
      .select('user_roles.user_role_id')
      .andWhere('user_roles.user_role_name = :type', { type: `${userRole}` })
      .limit(1)
      .execute();

    return JSON.parse(JSON.stringify(userRoleId))[0].user_roles_user_role_id;
  }

  async getDefaultCityId(defaultValue: string): Promise<Cities> {
    const defaultCity = await getConnection()
      .createQueryBuilder(Cities, 'cities')
      .select('cities.city_name_id')
      .andWhere('cities.city_name = :name', { name: `${defaultValue}` })
      .limit(1)
      .execute();

    return JSON.parse(JSON.stringify(defaultCity))[0].cities_city_name_id;
  }

  async getDefaultGenderTypeId(defaultValue: string): Promise<GenderTypes> {
    const defaultGenderValueId = await getConnection()
      .createQueryBuilder(GenderTypes, 'gender_types')
      .select('gender_types.gender_type_id')
      .andWhere('gender_types.gender_type = :type', { type: `${defaultValue}` })
      .limit(1)
      .execute();

    return JSON.parse(JSON.stringify(defaultGenderValueId))[0]
      .gender_types_gender_type_id;
  }
}
