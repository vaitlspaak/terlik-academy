import { Application } from './applications.entity';
import { UserRoles } from './user-roles.entity';
import { Cities } from './cities-db.entity';
import { GenderTypes } from './gender-types.entity';
import {
  Column,
  PrimaryGeneratedColumn,
  Entity,
  CreateDateColumn,
  JoinColumn,
  ManyToMany,
  ManyToOne,
} from 'typeorm';
import { IsEmail } from 'class-validator';

@Entity({
  name: 'users',
})
export class User {
  @PrimaryGeneratedColumn('uuid')
  @ManyToMany(type => Application, application => application.user_id)
  user_id: Application[];

  @Column('nvarchar', { length: 45 })
  first_name: string;

  @Column('nvarchar', {
    nullable: true,
    length: 45,
  })
  middle_name: string;

  @Column('nvarchar', { length: 45 })
  last_name: string;

  @Column('varchar', { default: null })
  @ManyToOne(type => GenderTypes, gender => gender.gender_type_id, {
    cascade: true,
  })
  @JoinColumn({ name: 'gender_id' })
  gender_id: GenderTypes;

  @Column('date', { default: null })
  date_of_birth: Date;

  @Column('varchar', { length: 100, unique: true })
  @IsEmail()
  email: string;

  @Column('varchar', { length: 15, default: null })
  mobile_number: string;

  @Column('varchar', { nullable: true, default: null })
  @ManyToOne(type => Cities, city => city.city_name_id, {
    cascade: true,
  })
  @JoinColumn({ name: 'city_id' })
  city_id: Cities;

  @Column('date')
  @CreateDateColumn()
  registration_date: Date;

  @Column('varchar', { default: null })
  @ManyToOne(type => UserRoles, userRole => userRole.user_role_id, {
    cascade: true,
  })
  @JoinColumn({ name: 'user_role_id' })
  user_role_id: UserRoles;

  @Column('varchar', { select: false })
  password: string;

  @Column('varchar')
  avatarUrl: string;
}
