import { User } from './user.entity';
import {
  Column,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity({
  name: 'cities',
})
export class Cities {
  @PrimaryGeneratedColumn('uuid')
  @OneToMany(type => User, user => user.city_id)
  city_name_id: User[];

  @Column('varchar', { length: 45 })
  city_name: string;
}
