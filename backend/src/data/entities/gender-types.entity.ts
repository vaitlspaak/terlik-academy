import { Injectable } from '@nestjs/common';
import { User } from './user.entity';
import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  OneToMany,
} from 'typeorm';
@Injectable()
@Entity({
  name: 'gender_types',
})
export class GenderTypes {
  @PrimaryGeneratedColumn('uuid')
  @OneToMany(type => User, user => user.gender_id)
  gender_type_id: User[];

  @Column('varchar', { length: 20, unique: true })
  gender_type: string;
}
