import { Application } from './applications.entity';
import {
  Column,
  PrimaryGeneratedColumn,
  Entity,
  ManyToMany,
} from 'typeorm';

@Entity({
  name: 'application_data',
})
export class ApplicationData {
  @PrimaryGeneratedColumn('uuid')
  @ManyToMany(
    type => Application,
    application => application.applicationData_id,
  )
  applicationData_id: Application[];

  @Column('date', { default: null })
  exam_date: Date;

  @Column('int', { default: null })
  exam_result: number;

  @Column('varchar', { length: 100 })
  why_appling: string;

  @Column('varchar', { length: 100 })
  career_goals: string;

  @Column('varchar')
  resume: string;
}
