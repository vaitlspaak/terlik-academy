import { User } from './user.entity';
import { ApplicationData } from './application-data.entity';
import { Course } from './course.entity';
import {
  PrimaryGeneratedColumn,
  ManyToMany,
  JoinTable,
  Entity,
  Column,
  CreateDateColumn,
} from 'typeorm';

@Entity({
  name: 'applications',
})
export class Application {
  @PrimaryGeneratedColumn('uuid')
  application_id: string;

  @ManyToMany(type => User, users => users.user_id)
  @JoinTable()
  user_id: User[];

  @ManyToMany(type => Course, course => course.course_id)
  @JoinTable()
  course_id: Course[];

  @ManyToMany(
    type => ApplicationData,
    applicationData => applicationData.applicationData_id,
  )
  @JoinTable()
  applicationData_id: ApplicationData[];

  @Column('date')
  @CreateDateColumn()
  application_date: Date;
}
