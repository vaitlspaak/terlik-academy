import { Application } from './applications.entity';
import { Column, PrimaryGeneratedColumn, Entity, ManyToMany } from 'typeorm';

@Entity({
  name: 'courses',
})
export class Course {
  @PrimaryGeneratedColumn('uuid')
  @ManyToMany(
    type => Application,
    application => application.applicationData_id,
  )
  course_id: Application[];

  @Column('varchar', { length: 30 })
  course_name: string;

  @Column('varchar', { length: 100 })
  course_description: string;

  @Column('tinyint')
  active: boolean;
}
