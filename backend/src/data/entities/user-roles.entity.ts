import { Injectable } from '@nestjs/common';
import { User } from './user.entity';
import {
  Column,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
@Injectable()
@Entity({
  name: 'user_roles',
})
export class UserRoles {
  @PrimaryGeneratedColumn('uuid')
  @OneToMany(type => User, user => user.user_role_id)
  user_role_id: User[];

  @Column('varchar', { length: 45, unique: true })
  user_role_name: string;
}
