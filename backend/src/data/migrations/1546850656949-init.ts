import {MigrationInterface, QueryRunner} from "typeorm";

export class init1546850656949 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("CREATE TABLE `user_roles` (`user_role_id` varchar(255) NOT NULL, `user_role_name` varchar(45) NOT NULL, UNIQUE INDEX `IDX_2df34f88f97ba1b4f2c0d24e44` (`user_role_name`), PRIMARY KEY (`user_role_id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `cities` (`city_name_id` varchar(255) NOT NULL, `city_name` varchar(45) NOT NULL, PRIMARY KEY (`city_name_id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `gender_types` (`gender_type_id` varchar(255) NOT NULL, `gender_type` varchar(20) NOT NULL, UNIQUE INDEX `IDX_f5329969a3485bb5c93082cca4` (`gender_type`), PRIMARY KEY (`gender_type_id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `users` (`user_id` varchar(255) NOT NULL, `first_name` varchar(45) NOT NULL, `middle_name` varchar(45) NULL, `last_name` varchar(45) NOT NULL, `gender_id` varchar(255) NULL, `date_of_birth` date NULL, `email` varchar(100) NOT NULL, `mobile_number` varchar(15) NULL, `city_id` varchar(255) NULL, `registration_date` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `user_role_id` varchar(255) NULL, `password` varchar(255) NOT NULL, `avatarUrl` varchar(255) NOT NULL, UNIQUE INDEX `IDX_97672ac88f789774dd47f7c8be` (`email`), PRIMARY KEY (`user_id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `courses` (`course_id` varchar(255) NOT NULL, `course_name` varchar(30) NOT NULL, `course_description` varchar(100) NOT NULL, `active` tinyint NOT NULL, PRIMARY KEY (`course_id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `applications` (`application_id` varchar(255) NOT NULL, `application_date` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), PRIMARY KEY (`application_id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `application_data` (`applicationData_id` varchar(255) NOT NULL, `exam_date` date NULL, `exam_result` int NULL, `why_appling` varchar(100) NOT NULL, `career_goals` varchar(100) NOT NULL, `resume` varchar(255) NOT NULL, PRIMARY KEY (`applicationData_id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `applications_user_id_users` (`applicationsApplicationId` varchar(255) NOT NULL, `usersUserId` varchar(255) NOT NULL, PRIMARY KEY (`applicationsApplicationId`, `usersUserId`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `applications_course_id_courses` (`applicationsApplicationId` varchar(255) NOT NULL, `coursesCourseId` varchar(255) NOT NULL, PRIMARY KEY (`applicationsApplicationId`, `coursesCourseId`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `applications_application_data_id_application_data` (`applicationsApplicationId` varchar(255) NOT NULL, `applicationDataApplicationDataId` varchar(255) NOT NULL, PRIMARY KEY (`applicationsApplicationId`, `applicationDataApplicationDataId`)) ENGINE=InnoDB");
        await queryRunner.query("ALTER TABLE `users` ADD CONSTRAINT `FK_7fc7be0985c48fa965c80fc8775` FOREIGN KEY (`gender_id`) REFERENCES `gender_types`(`gender_type_id`)");
        await queryRunner.query("ALTER TABLE `users` ADD CONSTRAINT `FK_03934bca2709003c5f08fd436d2` FOREIGN KEY (`city_id`) REFERENCES `cities`(`city_name_id`)");
        await queryRunner.query("ALTER TABLE `users` ADD CONSTRAINT `FK_6a9764cbcd911700611b8bfa44b` FOREIGN KEY (`user_role_id`) REFERENCES `user_roles`(`user_role_id`)");
        await queryRunner.query("ALTER TABLE `applications_user_id_users` ADD CONSTRAINT `FK_76f85d2e5c8f1e9b30ea1a06807` FOREIGN KEY (`applicationsApplicationId`) REFERENCES `applications`(`application_id`) ON DELETE CASCADE");
        await queryRunner.query("ALTER TABLE `applications_user_id_users` ADD CONSTRAINT `FK_ffa155c49e0bf579c8f8202820e` FOREIGN KEY (`usersUserId`) REFERENCES `users`(`user_id`) ON DELETE CASCADE");
        await queryRunner.query("ALTER TABLE `applications_course_id_courses` ADD CONSTRAINT `FK_738484f2ae2fe2323a2aa827c73` FOREIGN KEY (`applicationsApplicationId`) REFERENCES `applications`(`application_id`) ON DELETE CASCADE");
        await queryRunner.query("ALTER TABLE `applications_course_id_courses` ADD CONSTRAINT `FK_7dae1c107683f3d7c4fe6157756` FOREIGN KEY (`coursesCourseId`) REFERENCES `courses`(`course_id`) ON DELETE CASCADE");
        await queryRunner.query("ALTER TABLE `applications_application_data_id_application_data` ADD CONSTRAINT `FK_045331124304fcb0d57907231c7` FOREIGN KEY (`applicationsApplicationId`) REFERENCES `applications`(`application_id`) ON DELETE CASCADE");
        await queryRunner.query("ALTER TABLE `applications_application_data_id_application_data` ADD CONSTRAINT `FK_8282bbed1ae04bbd24f3370097a` FOREIGN KEY (`applicationDataApplicationDataId`) REFERENCES `application_data`(`applicationData_id`) ON DELETE CASCADE");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `applications_application_data_id_application_data` DROP FOREIGN KEY `FK_8282bbed1ae04bbd24f3370097a`");
        await queryRunner.query("ALTER TABLE `applications_application_data_id_application_data` DROP FOREIGN KEY `FK_045331124304fcb0d57907231c7`");
        await queryRunner.query("ALTER TABLE `applications_course_id_courses` DROP FOREIGN KEY `FK_7dae1c107683f3d7c4fe6157756`");
        await queryRunner.query("ALTER TABLE `applications_course_id_courses` DROP FOREIGN KEY `FK_738484f2ae2fe2323a2aa827c73`");
        await queryRunner.query("ALTER TABLE `applications_user_id_users` DROP FOREIGN KEY `FK_ffa155c49e0bf579c8f8202820e`");
        await queryRunner.query("ALTER TABLE `applications_user_id_users` DROP FOREIGN KEY `FK_76f85d2e5c8f1e9b30ea1a06807`");
        await queryRunner.query("ALTER TABLE `users` DROP FOREIGN KEY `FK_6a9764cbcd911700611b8bfa44b`");
        await queryRunner.query("ALTER TABLE `users` DROP FOREIGN KEY `FK_03934bca2709003c5f08fd436d2`");
        await queryRunner.query("ALTER TABLE `users` DROP FOREIGN KEY `FK_7fc7be0985c48fa965c80fc8775`");
        await queryRunner.query("DROP TABLE `applications_application_data_id_application_data`");
        await queryRunner.query("DROP TABLE `applications_course_id_courses`");
        await queryRunner.query("DROP TABLE `applications_user_id_users`");
        await queryRunner.query("DROP TABLE `application_data`");
        await queryRunner.query("DROP TABLE `applications`");
        await queryRunner.query("DROP TABLE `courses`");
        await queryRunner.query("DROP INDEX `IDX_97672ac88f789774dd47f7c8be` ON `users`");
        await queryRunner.query("DROP TABLE `users`");
        await queryRunner.query("DROP INDEX `IDX_f5329969a3485bb5c93082cca4` ON `gender_types`");
        await queryRunner.query("DROP TABLE `gender_types`");
        await queryRunner.query("DROP TABLE `cities`");
        await queryRunner.query("DROP INDEX `IDX_2df34f88f97ba1b4f2c0d24e44` ON `user_roles`");
        await queryRunner.query("DROP TABLE `user_roles`");
    }

}
