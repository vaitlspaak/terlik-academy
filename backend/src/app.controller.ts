import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';

@Controller('')
export class AppController {

  constructor(private readonly appService: AppService) { }

  @Get('')
  // @UseGuards(AuthGuard(), AdminGuard)
  mainRoute() {
    return this.appService.mainRoute();
  }

  @Get('register')
  registerRoute() {
    return this.appService.registerRoute();
  }

  @Get('login')
  loginRoute() {
    return this.appService.loginRoute();
  }
}
