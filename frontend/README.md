# TERLIK ACADEMY
## Knitting Stitches Academy Training APP

![Terlik Academy](http://www.survival-rpg.com/src/styles/img/art/Terlik-Academy-Logo.jpg)

## Functionalities of the APP:
### Users who can register and apply for different types of courses in the APP.
### Trainers who has admin access for the APP.
### SQL Database of users and admins for the APP.
--------------------------------------------------------------------------------
## Types of courses:
### 1 Knitting Stitches Fundamentals:
#### Content of this course:
 - Different types of Yarn
 - Knitting Needles 'My best frients'
 - Scissors 'Why shoud we care about them'

### 2 Knitting Stitches Advanced:
#### Content of this course:
 - Purl stitches
 - Combinations of two or more knitting techniques
 - Creating the stitch patterns

### 3 Knitting Stitches Patterns:
#### Content of this course:
 - GARTER STITCH PATTERN
 - STOCKINETTE STITCH PATTERN
 - RIB STITCH PATTERN
 - SEED STITCH PATTERN
 - ANDALUSIAN STITCH PATTERN
 - PIQUE RIB STITCH PATTERN
 - DOUBLE MOSS STITCH PATTERN
 - WAFFLE STITCH PATTERN
 - CHEVRON SEED STITCH PATTERN
 - DIAGONAL RIB STITCH PATTERN
 - FLAG STITCH PATTERN
 - DIAMOND BROCADE STITCH PATTERN
 - BASKET WEAVE STITCH PATTERN
 - GARTER CHECKERBOARD STITCH PATTERN
 - CATERPILLAR STITCH PATTERN
 - DIAGONAL CHEVRON ZIGZAG STITCH PATTERN
 - TRIANGLE STITCH PATTERN
 - REVERSE RIDGE STITCH PATTERN
 - IRISH MOSS STITCH PATTERN
 - HURDLE STITCH PATTERN


### Trainers who has admin access in this APP:
- baba Pena
- baba Tonka
- baba Elena

-------------------------------------------------------------------

### Team Members of this project: 
- Danail Kirilov (@my.telerikacademy.com - vaitlspaak@gmail.com)
- Petar Todorov (@my.telerikacademy.com - petar.ptodorov@gmail.com)

### Gitlab repository:
- Link: https://gitlab.com/vaitlspaak/terlik-academyt
