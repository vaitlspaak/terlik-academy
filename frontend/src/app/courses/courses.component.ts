import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.css']
})
export class CoursesComponent implements OnInit {

  public data: any;

  public constructor(private readonly http: HttpClient) {
    this.http
      .get('http://localhost:3000/courses')
      .subscribe(data => (this.data = data));
  }

  ngOnInit() {
  }

}
