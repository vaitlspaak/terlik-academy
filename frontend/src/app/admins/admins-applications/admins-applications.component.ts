import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admins-applications',
  templateUrl: './admins-applications.component.html',
  styleUrls: ['./admins-applications.component.css']
})
export class AdminsApplicationsComponent implements OnInit {

  public data: any;

  public constructor(private readonly http: HttpClient) {
    this.http
      .get('http://localhost:3000/admins/applications')
      .subscribe(data => (this.data = data));
  }

  ngOnInit() {
  }

}
