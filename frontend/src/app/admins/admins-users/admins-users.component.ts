import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admins',
  templateUrl: './admins-users.component.html',
  styleUrls: ['./admins-users.component.css']
})
export class AdminsUsersComponent implements OnInit {

  public data: any;

  public constructor(private readonly http: HttpClient) {
    this.http
      .get('http://localhost:3000/users')
      .subscribe(data => (this.data = data));
  }
  ngOnInit() {
  }

}
