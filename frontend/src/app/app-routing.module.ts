import { HomeComponent } from './home/home.component';
import { Notfound404Component } from './notfound404/notfound404.component';
import { AdminsUsersComponent } from './admins/admins-users/admins-users.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CoursesComponent } from './courses/courses.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { AdminsApplicationsComponent } from './admins/admins-applications/admins-applications.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'courses', component: CoursesComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'login', component: LoginComponent },
  { path: 'admins/users', component: AdminsUsersComponent },
  { path: 'admins/applications', component: AdminsApplicationsComponent },
  { path: '404', component: Notfound404Component },
  { path: '**', redirectTo: '404'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
