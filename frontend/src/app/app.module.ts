import { CoursesComponent } from './courses/courses.component';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AdminsUsersComponent } from './admins/admins-users/admins-users.component';
import { Notfound404Component } from './notfound404/notfound404.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { NavbarComponent } from './navbar/navbar.component';
import { AdminsApplicationsComponent } from './admins/admins-applications/admins-applications.component';

@NgModule({
  declarations: [
    AppComponent,
    CoursesComponent,
    Notfound404Component,
    HomeComponent,
    LoginComponent,
    RegisterComponent,
    NavbarComponent,
    AdminsApplicationsComponent,
    AdminsUsersComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [AppComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
